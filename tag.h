#include<QtCore>
#ifndef TAG_H
#define TAG_H

class Tag {
 public:
  Tag(): tag_name(""), cur_child(childs.begin()), cur_attrib(attribs.begin()) {}

 private:
  QString tag_name;
  QVector<QString> childs;
  QVector<QString>::iterator cur_child;
  QMap<QString, QString> attribs;
  QMap<QString, QString>::iterator cur_attrib;
};

#endif // TAG_H
