#include "tag.h"
#include<QtCore>
#ifndef PARSER_H
#define PARSER_H

class Parser{
public:
    virtual void lexical_parsing(QString str) =0;

};

class HTMLParser : public Parser {
public:
    HTMLParser(): HTML(""), cur_tag(tags.begin()) {}
    void loadHTMLFile(QString path);
    QString getHTML();
    void replaceEnters();
    void delTabs();
    void lexical_parsing(QString str);
private:
    QString HTML;
    enum statusLexPars {DATA, OPENED_TAG, NAME_TAG} lex_status;
    enum statusSyntPars {START, BEBORE_HTML, BEFORE_HEAD, IN_HEAD, AFTER_HEAD, IN_BODY,
                         AFTER_BODY, AFTER_AFTER_BODY} synt_status;
   QVector<Tag> tags;
   QVector<Tag>::iterator cur_tag;
};

#endif // PARSER_H
