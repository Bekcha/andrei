#include "parser.h"
#include<QtCore>

void HTMLParser::loadHTMLFile(QString path)
{
    QFile HTMLFile;
    HTMLFile.setFileName(path);
    HTMLFile.open(QIODevice::ReadOnly);
    if(HTMLFile.isOpen())
    {

        HTML = HTMLFile.readAll();
}
    HTMLFile.close();
}


QString HTMLParser::getHTML()
{
    return HTML;
}

void HTMLParser::replaceEnters()
{
    HTML.replace("\r\n", "");
}

void HTMLParser::delTabs()
{
    QStringList words = HTML.split(" ");
    QStringList::iterator strings = words.begin();
    while(strings != words.end())
    {
        if(*strings == "") words.erase(strings);
        if(*strings != "") ++strings;

    }

    HTML = words.join(" ");
}

void HTMLParser::lexical_parsing(QString str)
{


}
