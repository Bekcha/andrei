#include <QCoreApplication>
#include<QtCore>
#include "parser.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    HTMLParser pars;
    pars.loadHTMLFile("File.txt");
    qDebug() << pars.getHTML();
    pars.replaceEnters();
    pars.delTabs();
    qDebug() << pars.getHTML();

    return a.exec();
}
